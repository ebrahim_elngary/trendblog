import { StyleSheet, Dimensions } from 'react-native';
import { COLORS } from '../../common';
const { width, height } = Dimensions.get("window")

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    header: {
        height: 50,
        backgroundColor: '#72F8FC',
        width: width,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center',
        padding: 5,
        marginTop: 30,
    },
    image: {
        height: height * 0.62,
        width: width,
    },

    imageIcons: {
        width: width,
        height: 50,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center',
        paddingHorizontal: 5,
    },

    appName: {
        alignItems: "center",
        paddingTop: 20,
        justifyContent: 'center'
    },
    appNameText: {
        color: COLORS.main,
        fontSize: 25,
        fontWeight: 'bold',

    },
    imageName: {
        fontWeight: '600',
        fontSize: 14,
        marginLeft: 10
    },
    icons: {
        width: '40%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: "center",
    },
    nameStar: {
        width: "99%",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "flex-end",
    },
    iconNameViewStyle: {
        width: 0,
        justifyContent: 'center'
    },
    tabBottomBar: {
        height: 50,
        width: width,
        bottom: 0,
        right: 0,
        left: 0,
        position: 'absolute',
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: 'center',
        borderWidth: 0.5,
        borderColor: "#ddd",
        backgroundColor: '#fff',
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0
    }
})

export default styles;