import React, { Component } from 'react';
import { View, Text, Share, ImageBackground, KeyboardAvoidingView, Dimensions, Animated, Platform, Modal } from 'react-native';
import styles from './styles';
import StarRating from 'react-native-star-rating';
import { PressedIcon, ImagePicker, Button, LoadingModal } from '../../components'
import { COLORS, IMAGE } from '../../common';
import ActionSheet from 'react-native-actionsheet'
import { TextInput, ScrollView, FlatList } from 'react-native-gesture-handler';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { navigationOptions } from 'react-navigation'
import { getPost, ratePost, add_to_favorite, remove_post, fetch_categories, add_comment, top_rate, block_post, report_post } from '../../redux/actions'

const { width, height } = Dimensions.get("window")

class Home extends Component {
  static navigationOptions = () => {
    return {
      header: null,
      headerLayoutPreset: 'center'
    }
  }
  state = {
    image: null,
    imageUpload: null,
    rating: 0,
    visible: false,
    modalVisible: false,
    isloading: true,
    moreDown: ["invite Freinds"],
    commentPress: false,
    comment: ""
  };

  componentDidMount() {
    this.props.getPost()
    this.props.fetch_categories()
    this.props.top_rate()
    this.props.block_post()
    this.props.report_post()
  }

  componentWillReceiveProps() {
    setState({ comment: "" })
  }

  toggleImagePicker = () => {
    this.setState({ visible: !this.state.visible })
  }

  getImage = async (image) => {
    this.setState({
      imageUpload: image.base64,
      visible: false
    })
    this.props.navigation.navigate("PostPageScreen", { photo: image.uri })
  }

  onPressShare = () => {
    Share.share({
      message: "Welcome To TrendBlog App , www.hm.com",
      title: "TrendBlog",
      url: "www.hm.com"
    });
  }

  onPressRate = (value, id) => {
    this.setState({ rating: value });
    this.props.ratePost({ value, post_id: id })
  }
  onPressCategory = (index) => {
    const id = this.props.categories[index].id
    this.props.fetch_categories(id)
  }

  onPressTopRate = (id) => {
    this.props.top_rate(id)
  }

  onPressMoreTop = (index) => {
    const { id } = this.props.post;
    if (index == 0) {
      this.props.block_post(id)
    } else {
      this.props.report_post(id)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.post !== nextProps.post) {
      this.setState({ rating: nextProps.post.rating })
    }
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    const { post, loading, favorite, categories, data } = this.props;
    const isFavorite = post && post.id ? favorite.indexOf(post.id) != -1 : false
    const categories_list = categories.length > 0 ? categories.map(item => item.name) : []
    return (
      <View style={styles.container}>
        <View style={styles.appName}>
          <Text style={styles.appNameText}>TRENDBLOG</Text>
        </View>
        <Animated.ScrollView
          contentContainerStyle={{ height: Platform.OS == "android" ? height + 200 : null, flex: Platform.OS == "ios" ? 1 : null }}
          showsVerticalScrollIndicator={false}
          keyboardDismissMode={'on-drag'}
          snapToInterval={1000}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{
              nativeEvent: {
                contentOffset: {
                  x: 100,
                }
              }
            }]
          )}
          alwaysBounceVertical={true}
        // onMomentumScrollEnd={() => alert("Rate this post First")}
        >
          {
            !loading && post && post.img_url ?
              <View style={styles.image}>
                <ImageBackground
                  style={styles.image}
                  source={{ uri: post.img_url }} >
                  <View style={{ flexDirection: 'row', position: 'absolute', right: 10, top: 10 }}>
                    <PressedIcon source={IMAGE.favoraite}
                      style={{ marginRight: 5 }}
                      imageStyle={{ tintColor: isFavorite ? COLORS.main : "#000" }} onPress={() => this.props.add_to_favorite({ id: post.id })} />
                    <PressedIcon source={IMAGE.moreVertival} onPress={() => this.MoreTopActionSheet.show()} />
                  </View>
                  <View style={{ position: "absolute", alignItems: 'center', justifyContent: 'center', height: 40, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(0,0,0,0.5)' }} >
                    <Text style={{ color: "#fff", fontWeight: "700" }} >{post.caption}</Text>
                  </View>
                </ImageBackground>
              </View>
              :
              <View style={{ flex: 1 }} >
                <LoadingModal visible={loading} />
              </View>
          }
          <View style={styles.imageIcons}>
            <View style={styles.icons} >
              <PressedIcon source={IMAGE.delete}
                onPress={() => this.props.ratePost({ value: 0, post_id: post.id })} />
              <PressedIcon source={IMAGE.comment}
                onPress={() => this.setState({ commentPress: !this.state.commentPress })}
                imageStyle={{ tintColor: this.state.commentPress ? COLORS.main : "#000" }} />
              <PressedIcon source={IMAGE.share} onPress={() => this.onPressShare()} />
            </View>
            <StarRating
              disabled={false}
              maxStars={5}
              starSize={25}
              rating={Number(this.state.rating)}
              starStyle={{ margin: 3 }}
              fullStarColor={"gold"}
              selectedStar={(value) => this.onPressRate(value, post.id)}
            />
          </View>

          {
            this.state.commentPress &&
            <View>
              <View style={{ height: 45, backgroundColor: "#eee", width: width, justifyContent: "space-between", alignItems: 'center', flexDirection: 'row' }} >
                <TextInput
                  multiline={true}
                  maxLength={70}
                  placeholder={"Enter Your Comment"}
                  placeholderTextColor={"#aaa"}
                  onChangeText={(comment) => this.setState({ comment })}
                  value={this.state.comment}
                  style={{ width: "75%", marginHorizontal: 10, color: "#000" }}
                />
                <Button title={"Save"} style={{ borderWidth: 1, borderColor: "#000", borderRaduis: 10, marginRight: 10, width: "15%" }} titleStyle={{ color: "#000", fontWeight: '400' }}
                  onPress={() => {
                    this.props.add_comment({
                      id: this.props.post.id,
                      userComment: this.state.comment
                    })
                  }} />
              </View>
              <View>
                <Modal
                  animationType="slide"
                  transparent={false}
                  visible={this.state.modalVisible}
                  onRequestClose={() => {
                    Alert.alert('Closed');
                  }}>
                  <FlatList
                    contentContainerStyle={{ flex: 1 }}
                    data={this.props.post.comments}
                    renderItem={({ item }) => <Text>{item.data}</Text>}
                  />
                  <View style={{ backgroundColor: '#eee', width: "100%" }}>
                    <Button
                      title={"Close"}
                      style={{ alignItems: 'center', backgroundColor: '#eee', alignSelf: 'center' }}
                      onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                      }}
                    />
                  </View>
                </Modal>
                <Button title={"View all Comments"}
                  onPress={() => {
                    this.setModalVisible(true);
                  }}
                  style={{ width: width, height: 30, justifyContent: 'center', borderBottomWidth: 1, borderBottomColor: "#aaa", marginBotttom: 2 }} />
              </View>
            </View>
          }
          {
            post && post.img_url &&
            <ImageBackground source={{ uri: post.img_url }} style={{ width: width, height: height }} />
          }
        </Animated.ScrollView>

        <View style={styles.tabBottomBar}>
          <PressedIcon source={IMAGE.home} onPress={() => this.onPressTopRate()} />
          <PressedIcon source={IMAGE.search} onPress={() => this.CategoryActionSheet.show()} />
          <PressedIcon source={IMAGE.appIcon}
            imageStyle={{ height: 30, width: 30 }} onPress={() => this.toggleImagePicker()} />
          <PressedIcon source={IMAGE.profile}
            imageStyle={{ tintColor: "#000" }}
            onPress={() => this.props.navigation.navigate("ProfileScreen")} />
          <PressedIcon source={IMAGE.moreHorizontal}
            onPress={() => this.MoreDownActionSheet.show()}
            imageStyle={{ tintColor: "#000" }} />
        </View>
        <ActionSheet
          ref={o => this.MoreTopActionSheet = o}
          title={'Which one do you like ?'}
          options={["Block", "Report", 'Cancel']}
          cancelButtonIndex={2}
          destructiveButtonIndex={2}
          onPress={(index) => this.onPressMoreTop(index)}
        />
        <ActionSheet
          ref={o => this.CategoryActionSheet = o}
          title={'Choose Category'}
          options={[...categories_list, "Back"]}
          cancelButtonIndex={categories_list.length}
          destructiveButtonIndex={categories_list.length}
          onPress={(index) => this.onPressCategory(index)}
        />
        <ActionSheet
          ref={o => this.MoreDownActionSheet = o}
          title={'Which one do you like ? '}
          options={[...this.state.moreDown, 'Cancel']}
          cancelButtonIndex={this.state.moreDown.length}
          destructiveButtonIndex={this.state.moreDown.length}
          onPress={() => { () => this.props.navigation.navigate("HomeScreen") }}
        />
        <ImagePicker
          visible={this.state.visible}
          togglePicker={() => this.toggleImagePicker()}
          onSelectImage={this.getImage}
        />
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.home.loading,
    post: state.home.post,
    favorite: state.home.favorite,
    comment: state.home.comment,
    categories: state.home.categories
  }
}

const mapDispatshToProps = (dispatch) => {
  return bindActionCreators({
    getPost,
    ratePost,
    add_to_favorite,
    remove_post,
    fetch_categories,
    add_comment,
    top_rate,
    block_post,
    report_post
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatshToProps)(Home);