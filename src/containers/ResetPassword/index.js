import React, { Component } from 'react'
import { View, Text, ScrollView, TextInput, ImageBackground, TouchableOpacity, Image } from "react-native"
import styles from './styles'
import { Button, PressedIcon } from '../../components';
import { ICONS, COLORS, IMAGE } from '../../common';

class ResetPassword extends Component {
    state = {
        email: "",
        token: "",
        password: ""
    }
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <ImageBackground source={IMAGE.appBackground} style={{
                    flex: 1, alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <View style={{ position: 'absolute', left: 10, top: 40 }}>
                        <PressedIcon source={IMAGE.arrow} style={{ height: 50, width: 50 }} imageStyle={{ tintColor: "#FFF" }} onPress={() => this.props.navigation.goBack()} />
                    </View>

                    <View style={styles.logoView}>
                        <Text style={styles.profileText}>Reset Password</Text>
                    </View>

                    <View style={styles.email}>
                        <Image source={IMAGE.email} style={{ width: 25, height: 25, tintColor: '#fff' }} />
                        <TextInput
                            placeholder={"Enter Your Email"}
                            placeholderTextColor={COLORS.secoundery}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            keyboardType={"email-address"}
                            style={{ width: "85%", marginHorizontal: 10, color: COLORS.secoundery }}
                        />

                    </View>
                    <Text style={{ color: COLORS.secoundery, fontSize: 15 }}>Click send to receive a reset token</Text>
                    <View style={{ padding: 5, alignSelf: 'stretch', alignItems: 'center' }} >
                        <Button title={"SEND"} style={styles.loginButton} titleStyle={styles.loginButtonText} />
                    </View>
                    <View style={styles.token}>
                        <TextInput
                            placeholder={"Enter Your Token"}
                            placeholderTextColor={COLORS.secoundery}
                            onChangeText={(token) => this.setState({ token })}
                            value={this.state.token}
                            keyboardType={"email-address"}
                            style={{ width: "85%", marginHorizontal: 10, color: COLORS.secoundery }}

                        />
                    </View>
                    <View style={styles.password}>
                        <TextInput
                            placeholder={"Enter New Password"}
                            placeholderTextColor={COLORS.secoundery}
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                            keyboardType={"email-address"}
                            style={{ width: "85%", marginHorizontal: 10, color: COLORS.secoundery }}
                        />
                    </View>
                    <View style={{ padding: 5, alignSelf: 'stretch', alignItems: 'center' }} >
                        <Button title={"RESET PASSWORD"} style={styles.loginButton} titleStyle={styles.loginButtonText} />
                    </View>
                </ImageBackground>
            </ScrollView >
        )
    }
}

export default ResetPassword