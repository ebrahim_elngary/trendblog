import { StyleSheet } from 'react-native';
import { COLORS } from '../../common/Colors';

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center',

    },
    logoView: {
        width: '70%',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15
    },
    profileText: {
        fontSize: 27,
        color: COLORS.secoundery,
        alignSelf: 'center',
        justifyContent: "center",
        fontWeight: "bold",
        alignSelf: "center",
    },
    email: {
        borderBottomWidth: 1,
        borderBottomColor: COLORS.secoundery,
        width: "70%",
        flexDirection: "row",
        marginVertical: 50,
    },
    token: {
        borderBottomWidth: 1,
        marginVertical: 20,
        borderBottomColor: COLORS.secoundery,
        width: "70%",
    },
    password: {
        borderBottomWidth: 1,
        marginBottom: 20,
        borderBottomColor: COLORS.secoundery,
        width: "70%",
    },
    loginButton: {
        marginBottom: 10,
        borderWidth: 2,
        borderRadius: 15,
        width: "70%",
        height: 50,
        borderColor: COLORS.secoundery
    },
    loginButtonText: {
        color: COLORS.secoundery,
        fontSize: 20,

    },

})

export default styles;