import React, { Component } from 'react';
import { ImageBackground, AsyncStorage, View, Text } from 'react-native'
import { IMAGE } from '../../common';

class Splash extends Component {
    static navigationOptions = () => {
        return {
            header: null,
            headerLayoutPreset: 'center'
        }
    }
    async componentDidMount() {
        await AsyncStorage.getItem("@USER_TOKEN")
            .then(user => {

                if (user) {
                    this.props.navigation.navigate("HomeScreen")
                } else {
                    this.props.navigation.navigate("LoginScreen")
                }
            })

    }
    render() {
        return (
            <ImageBackground source={IMAGE.appBackground} style={{ flex: 1 }} />
        )
    }
}

export default Splash