import React, { Component } from 'react'
import { View, Text, ScrollView, TextInput, ImageBackground, Image } from "react-native"
import styles from './styles'
import { Button, PressedIcon, LoadingModal } from '../../components';
import { COLORS } from '../../common';
import { IMAGE } from '../../common/Images';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { signupWithEmail } from '../../redux/actions';

class SignUp extends Component {

    state = {
        email: "",
        password: "",
    }

    onPressSignup = () => {
        const { navigation } = this.props;
        const { email, password } = this.state;

        this.props.signupWithEmail({ email, password, navigation });
    }
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <ImageBackground source={IMAGE.appBackground} style={{
                    flex: 1, alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <View style={{ position: 'absolute', left: 10, top: 40 }}>
                        <PressedIcon source={IMAGE.arrow} style={{ height: 50, width: 50 }} imageStyle={{ tintColor: "#FFF" }} onPress={() => this.props.navigation.goBack()} />
                    </View>
                    <View style={{ padding: 5, alignSelf: 'stretch', alignItems: 'center' }} >
                        <View style={styles.logoArea}>

                            <Text style={styles.welcome}>REGISTER</Text>

                        </View>
                        <View style={{ marginVertical: 20 }}>

                            <View style={styles.email}>
                                <Image source={IMAGE.email} style={{ width: 25, height: 25, tintColor: '#fff' }} />
                                <TextInput
                                    placeholder={"Email"}
                                    placeholderTextColor={COLORS.secoundery}
                                    onChangeText={(email) => this.setState({ email })}
                                    value={this.state.email}
                                    keyboardType={"email-address"}
                                    style={{ width: "85%", color: "#fff", marginHorizontal: 10 }}
                                />

                            </View>
                            <View style={styles.password}>
                                <Image source={IMAGE.password} style={{ width: 25, height: 25, tintColor: '#fff' }} />

                                <TextInput
                                    placeholder={"Password"}
                                    placeholderTextColor={COLORS.secoundery}
                                    onChangeText={(password) => this.setState({ password })}
                                    value={this.state.password}
                                    secureTextEntry
                                    maxLength={30}
                                    style={{ width: "85%", color: "#fff", marginHorizontal: 10 }}

                                />
                            </View>
                        </View>

                        <View style={{ marginBottom: 5, alignItems: 'center', justifyContent: 'center' }} >
                            {this.props.error.length > 0 && <Text style={{ fontWeight: "bold", color: "red", padding: 5 }}>{this.props.error}</Text>}
                        </View>

                    </View>
                    <Button title={"Register"} style={styles.registerButton} titleStyle={styles.loginButtonText} onPress={this.onPressSignup} />
                    <LoadingModal visible={this.props.loading} />
                </ImageBackground>
            </ScrollView>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        signupWithEmail
    }, dispatch)
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)