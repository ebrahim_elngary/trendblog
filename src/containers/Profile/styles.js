import { StyleSheet } from 'react-native';
import { COLORS } from '../../common';

const styles = StyleSheet.create({
    container: {
        flex: 1

    },
    header: {
        height: 50,
        marginVertical: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between",
        backgroundColor: "#eee",
    },
    fullView: {
        height: "28%",

    },
    userName: {
        height: 40,
        backgroundColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center'
    },
    userNameText: {
        color: COLORS.main,
        textAlign: 'center',
        fontSize: 17,
        fontWeight: 'bold'
    },
    details: {
        height: "100%",
        flexDirection: 'row'
    },
    avatar: {
        width: 110,
        height: 110,
        borderRadius: 55,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        // backgroundColor:'#eee'
    },
    iconDeatails: {
        width: "65%",
        marginRight: 5,
    },
    icons: {
        flexDirection: 'row',
        height: 50,
        width: '100%',
        justifyContent: "space-around",
        borderWidth:0.5,
        alignItems:'center',
        borderColor:"#eee"
    },
    textView: {
        paddingHorizontal: 5
    },
    tabBar: {
        flexDirection: "row",
        height: 50,
        width: '100%',
        backgroundColor: "#eee",
        justifyContent: 'space-around',
        alignItems: 'center'
    }
})

export default styles;