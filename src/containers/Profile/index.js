import React, { Component } from 'react';
import { View, Text, Image, Dimensions, FlatList } from 'react-native';
import styles from './styles';
import { PressedIcon, LoadingModal } from '../../components'
import { IMAGE, COLORS } from '../../common';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getProfile, getPosts, getFavorite, getNews } from '../../redux/actions'
import { Tab, Tabs, TabHeading } from 'native-base';

const { width, height } = Dimensions.get("window")

const IconCount = ({ source, count, imageStyle }) => {
  return (
    <View style={{ flexDirection: 'row', alignItems: "center" }}>
      <Image source={source} style={[{ width: 25, height: 25 }, imageStyle]} />
      <Text style={{ marginLeft: 10, fontSize: 20 }}>{count}</Text>
    </View>
  )
}

const PostsCard = ({ item }) => {
  return (
    <View style={{ width: 100, height: 150, backgroundColor: 'green' }}>
      <Image source={IMAGE.search} style={{ width: "100%", height: '80%', backgroundColor: 'red' }} />
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', height: 50, width: '100%', backgroundColor: 'yellow' }}>
        <IconCount source={IMAGE.star} count={100} />
        <IconCount source={IMAGE.delete} count={1} />
        <IconCount source={IMAGE.favoraite} count={10} />
      </View>
    </View>
  )
}
const NewsCard = ({ item }) => {
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'space-around', height: 100, width: '100%', backgroundColor: 'yellow' }}>
      <Image source={IMAGE.home} style={{ width: "30%", height: 100 }} />
      <Text style={{ width: '60%' }}>fdfqwjdfwqfduqwudq</Text>
      <Text style={{ width: '30%' }}>30 OCT 2018</Text>
    </View>
  )
}

class Profile extends Component {
  static navigationOptions = () => {
    return {
      header: null,
      headerLayoutPreset: 'center'
    }
  }
  async componentDidMount() {
    await this.props.getProfile()
    await this.props.getPosts()
    await this.props.getFavorite()
    await this.props.getNews()

  }
  renderFavoriteItem = ({ item }) => {
    console.log(item)
    return <Image style={{ width: width / 3, height: width / 3, borderWidth: 0.5, borderColor: COLORS.main }} source={{ uri: item.post.img_url }} />
  }

  renderPostsItem = ({ item }) => {
    return <PostsCard item={item} />
  }

  renderNewsItem = ({ item }) => {
    return <NewsCard item={item} />
  }
  render() {
    console.log(this.props.favorite)
    const { profile, loading, username } = this.props;

    console.log(profile);
    return (
      !profile ?
        <View style={{ flex: 1 }} >
          <LoadingModal visible={loading} />
        </View> :
        <View style={styles.container}>
          <View style={styles.header}>
            <PressedIcon
              source={IMAGE.arrow} style={{ height: 40, width: 40 }}
              imageStyle={{ tintColor: "#000" }}
              onPress={() => this.props.navigation.goBack()} />
            <Text style={{ width: '70%', textAlign: 'center', fontSize: 17 }}>{profile.username}</Text>
            <PressedIcon
              source={IMAGE.menu}
              imageStyle={{ tintColor: '#000', height: 35, width: 35 }}
              onPress={() => this.props.navigation.toggleDrawer()}
            />
          </View>
          <View style={styles.fullView}>
            <View style={styles.details}>
              <Image style={styles.avatar} resizeMode={'contain'} source={{ uri: (profile && profile.img_url) ? profile.img_url : null }} />
              <View style={styles.iconDeatails}>
                <View style={styles.icons}>
                  <IconCount source={IMAGE.star} count={profile.rating} />
                  <IconCount source={IMAGE.delete} count={profile.trash} />
                  <View style={{ width: "30%", alignItems: 'center', justifyContent: 'center' }}>
                    <Text>Joined:</Text>
                    <Text>{profile.date}</Text>
                  </View>
                </View>
                <View style={styles.textView}>
                  <Text numberOfLines={5}>{profile.bio}</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 1 }} >
            <Tabs tabBarUnderlineStyle={{ backgroundColor: COLORS.main }} >
              <Tab heading={<TabHeading ><Image source={IMAGE.warn} style={{ width: 25, height: 25 }} /></TabHeading>}>
                <FlatList
                  data={this.props.news}
                  renderItem={this.renderNewsItem}
                  numColumns={1}
                  keyExtractor={item => JSON.stringify(item)}
                />
              </Tab>
              <Tab heading={<TabHeading><Image source={IMAGE.square} style={{ width: 30, height: 30, tintColor: "#000" }} /></TabHeading>}>
                <FlatList
                  data={this.props.posts}
                  renderItem={this.renderPostsItem}
                  numColumns={2}
                  keyExtractor={item => JSON.stringify(item)}

                />
              </Tab>
              <Tab heading={<TabHeading><Image source={IMAGE.favoraite} style={{ width: 25, height: 25, tintColor: "#000" }} /></TabHeading>}>
                <FlatList
                  data={this.props.favorite}
                  renderItem={this.renderFavoriteItem}
                  numColumns={3}
                  keyExtractor={item => JSON.stringify(item)}
                />
              </Tab>
            </Tabs>
          </View>

        </View>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getProfile,
    getPosts,
    getFavorite,
    getNews
  }, dispatch)
}

const mapStateToProps = state => {
  return {
    loading: state.profile.loading,
    profile: state.profile.profileData,
    news: state.profile.news,
    favorite: state.profile.favorite
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);

