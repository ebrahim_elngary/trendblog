import { StyleSheet } from 'react-native';
import { COLORS } from '../../common/Colors';

const styles = StyleSheet.create({
    container: {
       // flex: 1,
      //  backgroundColor: "gray",
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        height: "60%",
        width: "100%",
        //backgroundColor: "green",
    },
    saveButton: {
        width: 40,
        marginRight: 10,
        borderWidth: 1,
        borderColor: COLORS.secoundery,
        borderRadius: 12
    },
    post: {
        borderWidth: 1,
        height: 50,
         width: "70%",
        justifyContent: 'center',
        alignItems:'center',
        borderRadius: 15,
        marginVertical: 10,
        backgroundColor: '#eee'
    },
    chooseButton: {
        marginBottom: 10,
        borderWidth: 1,
        borderRadius: 15,
        width: "70%",
        height: 50,
    },
    uploadButton: {
        marginBottom: 10,
        borderRadius: 15,
        width: "70%",
        height: 50,
        backgroundColor: COLORS.main
        //borderColor: COLORS.secoundery
    },
    loginButtonText: {
        //color: COLORS.secoundery
    }
})

export default styles;