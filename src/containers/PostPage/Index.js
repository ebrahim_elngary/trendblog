import React, { Component } from 'react'
import { View, TextInput, KeyboardAvoidingView, Image } from "react-native"
import styles from './styles'
import { Button, PressedIcon } from '../../components'
import { COLORS, IMAGE } from '../../common'
import ActionSheet from 'react-native-actionsheet'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetch_categories, upload_post } from '../../redux/actions'
class PostPage extends Component {

    state = {
        postName: "",
        categoryId: null
    }

    componentDidMount() {
        this.props.fetch_categories()
    }
    onPressCategory = (index) => {
        const id = this.props.categories[index].id;
        this.setState({ categoryId: id })
    }

    onPressUpload = (imageUpload) => {
        this.props.upload_post({
            caption: this.state.postName, category: this.state.categoryId
            , imageFile: imageUpload, navigation: this.props.navigation
        })
    }

    render() {
        const { categories } = this.props;
        const imageUpload = this.props.navigation.getParam("photo", null)
        const categories_list = categories.length > 0 ? categories.map(item => item.name) : []

        return (
            <KeyboardAvoidingView behavior="padding" style={{ flex: 1, }}>
                <Image source={imageUpload ? { uri: imageUpload } : IMAGE.appIcon} resizeMode={"contain"} style={styles.image} />
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View style={styles.post}>
                        <TextInput
                            placeholder={"Enter Post Name"}
                            placeholderTextColor={COLORS.secoundery}
                            onChangeText={(postName) => this.setState({ postName })}
                            value={this.state.postName}
                            placeholderTextColor={"#000"}
                        />

                    </View>

                    <Button
                        title={"Choose Category"}
                        style={styles.chooseButton}
                        titleStyle={styles.loginButtonText}
                        onPress={() => this.CategoryActionSheet.show()} />

                    <ActionSheet
                        ref={o => this.CategoryActionSheet = o}
                        title={'Choose Category'}
                        options={[...categories_list, 'Cancel']}
                        cancelButtonIndex={categories_list.length}
                        destructiveButtonIndex={categories_list.length}
                        onPress={(index) => this.onPressCategory(index)}
                    />
                    <Button
                        title={"Upload"}
                        style={styles.uploadButton}
                        titleStyle={styles.loginButtonText}
                        onPress={() => this.onPressUpload(imageUpload)}
                    />
                </View>
            </KeyboardAvoidingView>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.home.loading,
        categories: state.home.categories

    }
}

const mapDispatshToProps = (dispatch) => {
    return bindActionCreators({
        fetch_categories,
        upload_post
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatshToProps)(PostPage)