import React, { Component } from 'react'
import { View, Text, ScrollView, ImageBackground, TouchableOpacity, Image } from "react-native"
import styles from './styles'
import { Button, PressedIcon, ImagePicker, LoadingModal } from '../../components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ICONS, COLORS, IMAGE } from '../../common';
import ActionSheet from 'react-native-actionsheet';
import { TextInput } from 'react-native-gesture-handler';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { completeSignUp,getSettingsData } from '../../redux/actions'
const Selected = ({ titleText, name, onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={onPress} style={{ width: 210, height: 40, borderColor: COLORS.secoundery, borderWidth: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 4, marginBottom: 10 }}>
      <Text style={{ fontSize: 20, fontWeight: "bold", color: COLORS.secoundery }}>{titleText}</Text>
      <Icon name={name} size={30} color={COLORS.secoundery} />
    </TouchableOpacity>
  )
}
class CompleteSignUp extends Component {
  state = {
    //Modal
    visible: false,
    image: null,
    //Data
    imageFile: null,
    username: "",
    gender: null,
    age: null,
    country: null,
  };
  componentDidMount() {
    this.props.getSettingsData()

  }
  showAgesActionSheet = () => {
    this.AgesActionSheet.show()
  }
  showCountriesActionSheet = () => {
    this.CountriesActionSheet.show()
  }
  toggleImagePicker = () => {
    this.setState({ visible: !this.state.visible })
  }
  getImage = async (image) => {
    this.setState({
      image: image.uri,
      imageFile: image.base64,
      visible: false
    })
  }
  onPressStart = () => {
    const { navigation } = this.props
    const { imageFile, username, gender, age, country } = this.state
    const userData = { imageFile, username, gender, age, country }
    this.props.completeSignUp({ userData, navigation })
  }
  render() {
    const { image, username } = this.state;
    const avatarImage = image ? { uri: image } : IMAGE.avatar;
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <ImageBackground source={IMAGE.appBackground} style={{
          flex: 1, alignItems: 'center',
          justifyContent: 'center'
        }}>
          <View style={{ position: 'absolute', left: 10, top: 40 }}>
            <PressedIcon
              source={IMAGE.arrow} style={{ height: 40, width: 40 }}
              imageStyle={{ tintColor: "#FFF" }}
              onPress={() => this.props.navigation.goBack()} />
          </View>

          <View style={styles.logoView}>
            <Text style={styles.profileText}>YOUR PROFILE</Text>
          </View>

          <TouchableOpacity
            onPress={() => this.toggleImagePicker()}
            style={styles.avatar}>
            <Image source={avatarImage}
              style={{ flex: 1, width: null, height: null, tintColor: !image ? '#fff' : null }} />
          </TouchableOpacity>
          <View style={styles.userNameView}>
            <TextInput style={styles.username}
              placeholder={"Enter Your Username"}
              placeholderTextColor={COLORS.secoundery}
              onChangeText={(username) => this.setState({ username })}
              value={username}
            />
            <View style={{ flexDirection: "row" }}>
              <View style={{
                height: 120, width: 120, borderWidth: 2, borderColor: "#FFF",
                borderBottomWidth: 0, borderLeftWidth: 0,
                backgroundColor: this.state.gender == "F" ? 'rgba(255,255,255,0.6)' : null,
                alignItems: 'center', justifyContent: 'center'
              }}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => this.setState({ gender: "F" })} >
                  <Icon name={ICONS.female} size={100} color={COLORS.secoundery} />
                </TouchableOpacity>
              </View>
              <View style={{
                height: 120, width: 120, borderWidth: 2, borderColor: "#FFF",
                borderBottomWidth: 0, borderRightWidth: 0, borderLeftWidth: 0,
                backgroundColor: this.state.gender == "M" ? 'rgba(255,255,255,0.6)' : null,
                alignItems: 'center', justifyContent: 'center',
              }}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => this.setState({ gender: "M" })} >
                  <Icon name={ICONS.male} size={100} color={COLORS.secoundery} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View>
            <Selected onPress={() => this.showAgesActionSheet()}
              name={ICONS.dropDown}
              titleText={this.state.age ? this.state.age : "Choose Your Age"} />
            <ActionSheet
              ref={o => this.AgesActionSheet = o}
              title={"Select your Age"}
              options={[...this.props.ages, 'Cancel']}
              cancelButtonIndex={this.props.ages.length}
              destructiveButtonIndex={this.props.ages.length}
              onPress={(index) => this.setState({ age: this.props.ages[index] })} />
          </View>
          <View>
            <Selected onPress={() => this.showCountriesActionSheet()}
              name={ICONS.dropDown}
              titleText={this.state.country ? this.state.country : "Choose Country"} />
            <ActionSheet
              ref={o => this.CountriesActionSheet = o}
              title={"Select your Country!"}
              options={[...this.props.countries, 'Cancel']}
              cancelButtonIndex={this.props.countries.length}
              destructiveButtonIndex={this.props.countries.length}
              onPress={(index) => this.setState({ country: this.props.countries[index] })} />
          </View>
          <View style={{ padding: 5, alignSelf: 'stretch', alignItems: 'center' }} >
            <Button
              title={"START"}
              style={styles.loginButton} titleStyle={styles.loginButtonText}
              onPress={this.onPressStart} />
          </View>
          <ImagePicker
            visible={this.state.visible}
            togglePicker={() => this.toggleImagePicker()}
            onSelectImage={this.getImage} />
            <LoadingModal visible={this.props.loading} />
        </ImageBackground>
      </ScrollView >
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    completeSignUp,
    getSettingsData
  }, dispatch)

}

const mapsStateToProps = state=>{
  return {
    loading : state.settings.loading,
    ages : state.settings.ages,
    countries : state.settings.countries
  }
}

export default connect(mapsStateToProps, mapDispatchToProps)(CompleteSignUp)