import { StyleSheet } from 'react-native';
import { COLORS } from '../../common/Colors';

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center'
    },
    logoView: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    profileText: {
        fontSize: 27,
        color: COLORS.secoundery,
        alignSelf: 'center',
        justifyContent: "center",
        fontWeight: "bold",
        alignSelf: "center"
    },
    avatar: {
        borderRadius: 60,
        borderWidth: 2,
        height: 120,
        width: 120,
        borderColor: COLORS.secoundery,
        overflow: 'hidden'
    },
    userNameView: {
        width: '40%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
        marginBottom:20
    },
    username: {
        color: COLORS.secoundery,
        alignSelf: 'center',
        justifyContent: "center",
        fontWeight: "bold",
        alignSelf: "center",
        width: "100%",
    },
    loginButton: {
        marginBottom: 10,
        borderWidth: 2,
        borderRadius: 15,
        width: "70%",
        height: 50,
        borderColor: COLORS.secoundery
    },
    loginButtonText: {
        color: COLORS.secoundery,
        fontSize: 20,

    },
    countryButton: {
        borderWidth: 1,
        borderColor: COLORS.secoundery,
        width: "80%"
    }

})

export default styles;