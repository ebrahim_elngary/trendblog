import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import { ICONS } from '../../common/Icons';
import { PressedIcon, Spinner } from '../../components'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTerms } from '../../redux/actions'

class Terms extends Component {
    static navigationOptions = () => {
        return {
            title: 'Terms & Conditions',
            tabBarIcon: <PressedIcon name={ICONS.favorite} size={20} />
        }
    }
    componentDidMount() {
        this.props.getTerms()
    }
    render() {
        return (<View style={styles.container}>
            {
                this.props.loading ?
                    <Spinner /> : <Text>{this.props.termsData}</Text>
            }
        </View>)
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        getTerms
    }, dispatch)
}

const mapStateToProps = state => {
    return {
        loading: state.terms.loading,
        termsData: state.terms.termsData
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Terms);

