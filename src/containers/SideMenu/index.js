import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from "react-native"
import { PressedIcon } from '../../components';
import { IMAGE, ICONS, COLORS } from '../../common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { user_logout } from '../../redux/actions'

const IconName = ({ name, iconName, size, onPress }) => {
    return (
        <TouchableOpacity onPress={onPress}
            style={{ flexDirection: 'row', padding: 5, marginTop: 10, justifyContent: 'center', alignItems: 'center', width: "100%", height: 50, backgroundColor: '#eee' }}>
            <Icon name={iconName} size={size || 30} />
            <Text style={{ marginLeft: 10, fontSize: 20 }}>{name}</Text>
        </TouchableOpacity>
    )
}

class SideMenu extends Component {

    render() {
        return (
            <View style={{ flex: 1, }}>
                <IconName name={"Logout"} iconName={ICONS.logOut} size={35} />
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        user_logout
    }, dispatch)
}

const mapStateToProps = state => {
    return {
        loading: state.profile.loading,

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
