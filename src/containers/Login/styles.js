import { StyleSheet } from 'react-native';
import { COLORS } from '../../common/Colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',

    },
    logoArea: {
        //  flex: 1,
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15
    },
    welcome: {
        fontSize: 27,
        color: COLORS.secoundery,
        alignSelf: 'center',
        fontWeight: "bold"
    },
    email: {
        borderBottomWidth: 1,
        borderBottomColor: COLORS.secoundery,
        width: "70%",
        flexDirection: "row",
        marginBottom: 20
    },
    password: {
        borderBottomWidth: 1,
        borderBottomColor: COLORS.secoundery,
        width: '70%',
        flexDirection: "row",
        marginBottom: 20


    },
    loginButton: {
        marginBottom: 10,
        borderWidth: 2,
        borderRadius: 15,
        width: "70%",
        height: 50,
        borderColor: COLORS.secoundery
    },
    loginButtonText: {
        color: COLORS.secoundery,
        fontSize: 20,

    },
    buttonArea: {
        flex: 1
    },
    socialLogins: {
        width: "80%",
        flexDirection: 'row',
        justifyContent: "space-between",
        marginVertical: 20,
    },
    facebook: {
        backgroundColor: COLORS.facebook,
        width: "30%",
        borderRadius: 20

    },
    twitter: {
        backgroundColor: COLORS.twitter,
        marginBottom: 10,
        width: "30%",
        borderRadius: 20


    },
    google: {
        backgroundColor: COLORS.google,
        marginBottom: 10,
        width: "30%",
        borderRadius: 20


    },
    forgetText: {
        color: COLORS.secoundery,
        textDecorationLine: 'underline'



    },
    description: {
        color: COLORS.secoundery,

    },
    termsCondition: {
        color: COLORS.facebook,

    },
    termsConditionText: {
        color: COLORS.secoundery,
        textDecorationLine: 'underline'
    },
    registerButton: {
        marginBottom: 10,
        borderWidth: 2,
        borderRadius: 15,
        width: "50%",
        height: 50,
        borderColor: COLORS.secoundery
    }
})

export default styles;