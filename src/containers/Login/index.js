import React, { Component } from 'react'
import { View, Text, ScrollView, TextInput, ImageBackground, Alert, Image } from "react-native"
import styles from './styles'
import { Button, LoadingModal } from '../../components';
import { COLORS } from '../../common';
import * as Expo from 'expo';
import { IMAGE } from '../../common/Images';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginWithEmail } from '../../redux/actions';

class Login extends Component {
  static navigationOptions = () => {
    return {
      header: null,
      headerLayoutPreset: 'center'
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      email: 'memo@mailinator.com',
      password: 'memo123',
      submitted: false,
    }
    this.handleUserlogin = this.handleUserlogin.bind(this);
  }

  //Facebook Login
  async  logInFB() {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Expo.Facebook.logInWithReadPermissionsAsync('629168360839520', {
        permissions: ['public_profile'],
      });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
        Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
        console.log(await response.json().email)
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

  _googleSignin = async () => {
    try {
      const result = await Expo.Google.logInAsync({
        androidClientId: '1008048297901-87snu5kmkkoi6m2fjoqvinrbiv7ai3ka.apps.googleusercontent.com',
        iosClientId: "1008048297901-e3h0qreekmufu7stcqcge1ua9etd4ija.apps.googleusercontent.com",
        scopes: ['profile', 'email'],
        // behavior: 'system'
      })
      this.setState({ googleLoading: true })
      if (result.type === 'success') {
        let userInfoResponse = await fetch(
          'https://www.googleapis.com/userinfo/v2/me',
          {
            headers: { Authorization: `Bearer ${result.accessToken}` }
          }
        )

        let json = await userInfoResponse.json()

        this.setState(
          {
            uid: json.id,
            provider: 'google_oauth2',
            fullname: json.name,
            email: json.email,
            avatar: json.picture,
            googleLoading: true
          },
          () => {
            this.props.navigation.navigate('AddMobileNumberScreen', {
              data: this.state
            })
            this.setState({ googleLoading: false })
          }
        )
      } else {
        this.setState({ googleLoading: false })
      }
    } catch (e) {
      return this.setState({ googleLoading: false }, () => console.log(e))
    }
  }


  // User Login with email

  handleUserlogin() {
    const { navigation } = this.props;
    const { email, password } = this.state;
    this.setState({ loading: true })
    this.props.loginWithEmail({ email, password, navigation });
  }

  componentWillReceiveProps() {
    this.setState({ loading: false })
  }

  render() {
    const { navigation, error, loading } = this.props;
    const { email, password, submitted } = this.state;
    // const { loading } = this.props.auth;

    const nextScreen = navigation.getParam('CompleteSignUpScreen', 'HomeScreen');

    return (
      <View style={styles.container}>
        <ImageBackground source={IMAGE.appBackground} style={{
          flex: 1, alignItems: 'center',
          justifyContent: 'center'
        }}>
          <View style={{ padding: 5, alignSelf: 'stretch', alignItems: 'center' }} >
            <View style={styles.logoArea}>

              <Text style={styles.welcome}>WELCOME TO</Text>
              <Text style={styles.welcome}>TRENDBLOG</Text>

            </View>
            <View style={{ marginVertical: 20 }}>

              <View style={styles.email}>
                <Image source={IMAGE.email} style={{ width: 25, height: 25, tintColor: '#fff' }} />
                <TextInput
                  ref='email'
                  required={submitted && email.length == 0}
                  placeholder={"Email"}
                  placeholderTextColor={COLORS.secoundery}
                  onChangeText={(email) => this.setState({ email })}
                  value={email}
                  returnKeyType={'next'}
                  onSubmitEditing={() => { this.refs.password.focus(); }}
                  keyboardType={"email-address"}
                  style={{ width: "85%", marginHorizontal: 10, color: COLORS.secoundery }}
                />

              </View>
              <View style={styles.password}>
                <Image source={IMAGE.password} style={{ width: 25, height: 25, tintColor: '#fff' }} />

                <TextInput
                  ref='password'
                  required={submitted && password.length == 0}
                  value={password}
                  placeholder={"Password"}
                  placeholderTextColor={COLORS.secoundery}
                  onChangeText={(password) => this.setState({ password })}
                  returnKeyType="done"
                  onSubmitEditing={() => this.handleUserlogin()}
                  secureTextEntry
                  maxLength={30}
                  style={{ width: "85%", marginHorizontal: 10, color: COLORS.secoundery }}
                />
              </View>
            </View>
            <View style={{ marginBottom: 5, alignItems: 'center', justifyContent: 'center' }} >
              {error.length > 0 && <Text style={{ fontWeight: "bold", color: "red", padding: 5 }}>{error}</Text>}
            </View>
            <Button
              onPress={() => this.handleUserlogin()}
              title={"Login"} style={styles.loginButton}
              titleStyle={styles.loginButtonText}
            />
            <Text style={styles.description}>or use:</Text>
            <View style={styles.socialLogins}>

              <Button title={"Facebook"} style={styles.facebook} titleStyle={{ color: "#FFF", fontWeight: "bold" }} onPress={this.logInFB.bind(this)} />
              <Button title={"Twitter"} style={styles.twitter} titleStyle={{ color: "#FFF", fontWeight: "bold" }} />
              <Button title={"Google"} style={styles.google} titleStyle={{ color: "#FFF", fontWeight: "bold" }}
              // onPress={this._googleSignin}
              />
            </View>
            <Button title={"forget password?"} titleStyle={styles.forgetText} onPress={() => this.props.navigation.navigate("ResetPasswordScreen")} />
            <Text style={styles.description}>By Clicking above to signup You Accepted The</Text>
            <Button title={"Terms & Conditions"} style={styles.termsCondition} titleStyle={styles.termsConditionText} onPress={() => this.props.navigation.navigate("TermsScreen")} />

          </View>
          <Button title={"Register"} style={styles.registerButton} titleStyle={styles.loginButtonText} onPress={() => this.props.navigation.navigate("SignUpScreen")} />
          <LoadingModal visible={loading} />
        </ImageBackground>
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    loginWithEmail
  }, dispatch)
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);