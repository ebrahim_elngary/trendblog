import _ from 'lodash';



export const loginValidation = ({ email, password }) => {
    if (email.length == 0) {
        return { status: "fail", message: "Please Enter E-mail" }
    }
    else if (password.length < 6) {
        return { status: "fail", message: "Invalid Password" }
    } else if (!isEmail(email)) {
        return { status: 'fail', message: "Invalid Email" }
    } else {
        return { status: 'success' }
    }
}

export const completeValdation = ({ imageFile, username, gender, age, country }) => {
    if (imageFile == null) {
        return { status: "fail", message: "Please Upload Your Image" }
    }
    else if (username.length == 0) {
        return { status: "fail", message: "Please Enter Username" }

    }
    else if (gender == null) {
        return { status: "fail", message: "Please Select Gender" }

    }
    else if (age == null) {
        return { status: "fail", message: "Please Select Age" }

    }

    else if (country == null) {
        return { status: "fail", message: "Please Select Country" }

    }
    else {
        return { status: 'success' }

    }

}

const isEmail = (email) => {
    let mailReg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return mailReg.test(String(email).toLowerCase());
}








