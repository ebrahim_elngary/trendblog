import { Platform } from 'react-native'
export const ICONS = {
    home: "home-outline",
    styles: "image-search-outline",
    profile: "account-box-outline",
    favorite: "heart-multiple-outline",
    more: "dots-horizontal",
    fashion: "tshirt-crew-outline",
    delete: "delete-restore",
    share: "share-outline",
    comment: "comment-text-multiple-outline",
    new: "new-box",
    star: "star-box",
    image: "image-plus",
    menu: "menu",
    morevertical: "dots-vertical",
    camera: "camera",
    email: "email-variant",
    password: "lock-question",
    google: "google-plus",
    twitter: "twitter",
    facebook: "facebook-box",
    female: "human-female",
    male: "human-male",
    cart: "shopping",
    logOut: "power",
    dropDown: "arrow-down-drop-circle-outline"

}