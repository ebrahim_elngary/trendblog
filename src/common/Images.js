export const IMAGE = {
appIcon:require("../assets/appIcon.png"),
arrow:require("../assets/arrow.png"),
comment:require("../assets/comment.png"),
delete:require("../assets/delete.png"),
email:require("../assets/email.png"),
favoraite:require("../assets/favoraite.png"),
home:require("../assets/home.png"),
menu:require("../assets/menu.png"),
moreVertival:require("../assets/moreVertical.png"),
moreHorizontal:require("../assets/moreHorizontal.png"),
password:require("../assets/password.png"),
profile:require("../assets/profile.png"),
avatar:require("../assets/avatar.png"),
search:require("../assets/search.png"),
share:require("../assets/share.png"),
star:require("../assets/star.png"),
appBackground:require("../assets/login_background.png"),
square:require("../assets/2-squares.png"),
warn:require("../assets/warn.png")

}