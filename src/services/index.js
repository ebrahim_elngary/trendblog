import { AsyncStorage } from 'react-native'
export const base_url = "https://glacial-reaches-36443.herokuapp.com/api";

const auth_request = async (target, body) => {
    const url = `${base_url}/${target}`
    try {
        const res = await fetch(url, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ ...body })
        });
        return { result: await res.json(), status: res.status }
    } catch (err) {
        console.log(err)
    }
}

const get_request = async (target, header = true) => {
    const url = `${base_url}/${target}`;
    const token = await AsyncStorage.getItem("@USER_TOKEN")
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-AUTH-TOKEN': JSON.parse(token)
    }
    try {
        const res = await fetch(url, { method: "GET", headers: header ? headers : null });
        return { result: await res.json(), status: res.status }
    } catch (err) {
        console.log(err)
    }
}

const request = async (target, body) => {
    const url = `${base_url}/${target}`
    const token = await AsyncStorage.getItem("@USER_TOKEN")
    try {
        const res = await fetch(url, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-AUTH-TOKEN': JSON.parse(token)
            },
            body: JSON.stringify(body)
        }
        );
        return { result: await res.json(), status: res.status }
    } catch (err) {
        console.log(err)
    }
}

export const login = async ({ email, password }) => {
    const target = 'user/login';
    return await auth_request(target, { email, password })
}

export const signup = async ({ email, password }) => {
    const target = 'user/register';
    return await auth_request(target, { email, password })
}
export const forget_password = async ({ email }) => {
    const target = 'user/forget'
    return await auth_request(target, { email })
}

export const reset_password = async ({ email, password, token }) => {
    const target = 'user/reset'
    return await auth_request(target, { email, password, token })
}

export const signupComplete = async (userData) => {
    const target = 'user/complete';
    const body = { ...userData };
    return await request(target, body)
}


export const settingsData = async () => {
    return await get_request("settings")
}

export const fetch_post = async () => {
    const target = `post`
    return await get_request(target)
}

export const rate_post = async ({ value, id }) => {
    const target = `post/${id}/rating`;
    return await request(target, { value })
}

export const terms = async () => {
    return await get_request("terms", false)
}

export const categories = async () => {
    return await get_request("category")
}


export const fetch_profile = async () => {
    return await get_request("user/profile")
}
export const fetch_posts = async () => {
    return await get_request("user/posts")
}

export const logout = async () => {
    return await get_request("user/logout")
}

export const fetch_favorite = async () => {
    return await get_request("user/favorite")
}

export const fetch_news = async () => {
    return await get_request("user/news")
}

export const addToFavorite = async ({ id }) => {
    const target = `post/${id}/favorite`;
    return await get_request(target)
}

export const addComment = async ({ id, userComment }) => {
    const target = `post/${id}/comment`;
    const body = { ...userComment.message }
    return await request(target, body)
}

export const topRate = async (id) => {
    const target = `post/top?category=${id}`;
    return await get_request(target)
}

export const uploadPost = async ({ caption, category, imageFile }) => {
    const target = `post`
    const body = { caption, category, imageFile }
    return await request(target, body)
}

export const blockPost = async (id) => {
    const target = `post/${id}/block`
    return await get_request(target)
}

export const reportPost = async (id) => {
    const target = `post/${id}/report`
    return await get_request(target)
}