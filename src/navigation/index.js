import React from 'react';
import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import Profile from '../containers/Profile';
import Home from '../containers/Home';
import Login from '../containers/Login';
import CompleteSignUp from '../containers/CompleteSignUp';
import PostPage from '../containers/PostPage/Index';
import ResetPassword from '../containers/ResetPassword';
import SignUp from "../containers/SignUp";
import Terms from '../containers/Terms';
import SideMenu from '../containers/SideMenu';
import Splash from '../containers/Splash';

const AppNavigator = createStackNavigator({
    SplashScreen: { screen: Splash },
    HomeScreen: { screen: Home },
    ProfileScreen: { screen: Profile },
    LoginScreen: { screen: Login },
    SignUpScreen: { screen: SignUp },
    CompleteSignUpScreen: { screen: CompleteSignUp },
    TermsScreen: { screen: Terms },
    PostPageScreen: { screen: PostPage },
    ResetPasswordScreen: { screen: ResetPassword },
    SideMenuScreen: { screen: SideMenu }
}, {
        initialRouteName: 'LoginScreen'
    }
)

const MyDrawerNavigator = createDrawerNavigator({
    Drawer: {
        screen: AppNavigator,
    },
},
    {
        contentComponent: SideMenu
    }
);

export default createAppContainer(MyDrawerNavigator);
