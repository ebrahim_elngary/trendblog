import { combineReducers } from 'redux';
import AuthReducer from './Auth';
import SettingsReducer from './Settings';
import TermsReducer from './Terms';
import HomeReducer from './Home';
import ProfileRducer from './Profile';

export default combineReducers({
   auth: AuthReducer,
   settings: SettingsReducer,
   terms: TermsReducer,
   home: HomeReducer,
   profile: ProfileRducer
});