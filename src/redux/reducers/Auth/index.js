import { LOADING_AUTH, LOGIN_SUCCESS, ERROR_AUTH, SIGNUP_SUCCESS } from "../../actions/actionTypes";


const INIT_STATE = {
    user: null,
    token: null,
    error: '',
    loading: false,
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOADING_AUTH:
            return { ...state, loading: true };
        case LOGIN_SUCCESS:
            return { ...state, loading: false, error: '' }
        case SIGNUP_SUCCESS:
            return { ...state, loading: false, error: action.payload }
        case ERROR_AUTH:
            return { ...state, loading: false, error: action.payload }
        default:
            return state;
    }
}