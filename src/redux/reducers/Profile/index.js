import {
    PROFILE_FETCHING,
    PROFILE_FETCHING_SUCCESS,
    PROFILE_FETCHING_FAIL,
    USER_LOGOUT_SUCCESS,
    USER_LOGOUT_FAIL,
    POSTS_FETCHING_SUCCESS,
    NEWS_FETCHING_SUCCESS,
    FAVORITE_FETCHING_SUCCESS
} from '../../actions/actionTypes'

const INIT_STATE = {
    loading: false,
    profileData: null,
    logout: false,
    posts: [],
    favorite: [],
    news: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PROFILE_FETCHING:
            return { ...state, loading: true };

        case PROFILE_FETCHING_SUCCESS:
            return { ...state, loading: false, profileData: action.payload };

        case PROFILE_FETCHING_FAIL:
            return { ...state, loading: false };

        case USER_LOGOUT_SUCCESS:
            return { ...state, loading: false }

        case USER_LOGOUT_FAIL:
            return { ...state, loading: true }

        case POSTS_FETCHING_SUCCESS:
            return {
                ...state, loading: false, posts: action.payload
            }

        case PROFILE_FETCHING_FAIL:
            return { ...state, loading: true }

        case FAVORITE_FETCHING_SUCCESS:
            return { ...state, loading: false, favorite: action.payload }

        case PROFILE_FETCHING_FAIL:
            return { ...state, loading: true }

        case NEWS_FETCHING_SUCCESS:
            return {
                ...state, loading: false, news: action.payload
            }
        case PROFILE_FETCHING_FAIL:
            return { ...state, loading: true }
        default:
            return state;

    }
}