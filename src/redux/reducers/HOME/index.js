import {
    HOME_FETCHIG,
    HOME_FETCHIG_SUCCESS,
    HOME_FETCHIG_FAIL,
    HOME_FAVORITE_SUCCESS,
    HOME_FAVORITE_FAIL,
    HOME_CATEGORIES_SUCCESS,
    HOME_CATEGORIES_FAIL,
    HOME_COMMENT_SUCCESS,
    HOME_COMMENT_FAIL,
    HOME_TOP_SUCCESS,
    HOME_TOP_FAIL
} from '../../actions/actionTypes'

const INIT_STATE = {
    loading: false,
    post: null,
    favorite: [],
    categories: [],
    comment: [],
    top: []

}
export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case HOME_FETCHIG:
            return { ...state, loading: true };

        case HOME_FETCHIG_SUCCESS:
            return { ...state, loading: false, post: action.payload }

        case HOME_FETCHIG_FAIL:
            return { ...state, loading: false }

        case HOME_FAVORITE_SUCCESS:
            return {
                ...state,
                loading: false,
                favorite: [...state.favorite, action.payload]
            }

        case HOME_FAVORITE_FAIL:
            return { ...state, loading: true }

        case HOME_CATEGORIES_SUCCESS:
            return {
                ...state,
                loading: false,
                categories: action.payload
            }
        case HOME_CATEGORIES_FAIL:
            return { ...state, loading: true }

        case HOME_COMMENT_SUCCESS:
            return {
                ...state,
                loading: false,
                comment: { ...state.comment, comment: action.payload.comments }
            }
        case HOME_COMMENT_FAIL:
            return {
                ...state, loading: true
            }
        case HOME_TOP_SUCCESS:
            return {
                ...state,
                loading: false,
                top: action.payload
            }

        case HOME_TOP_FAIL:
            return { ...state, loading: true }
        default:
            return state

    }
}