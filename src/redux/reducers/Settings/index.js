import { SETTINGS_FETCHING, SETTINGS_FETCHING_FAIL, SETTINGS_FETCHING_SUCCESS, SETTINGS_UPDATE_SUCCESS } from "../../actions/actionTypes";


const INIT_STATE = {

    loading: false,
    countries: [],
    ages: []
}



export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SETTINGS_FETCHING:
            return { ...state, loading: true };
        case SETTINGS_FETCHING_SUCCESS:
            return { ...state, loading: false,  countries : action.payload.country_list, ages : action.payload.age_list  }
        case SETTINGS_UPDATE_SUCCESS:
            return{...state, loading : false}
        case SETTINGS_FETCHING_FAIL:
            return { ...state, loading: false, error: action.payload }
        default:
            return state;
    }
}