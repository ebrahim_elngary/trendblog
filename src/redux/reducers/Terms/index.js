import { TERMS_FETCHING, TERMS_FETCHING_SUCCESS, TERMS_FETCHING_FAIL } from '../../actions/actionTypes'

const INIT_STATE = {
    loading: false,
    termsData: ""
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case TERMS_FETCHING:
            return { ...state, loading: true };

        case TERMS_FETCHING_SUCCESS:
            return { ...state, loading: false, termsData: action.payload };

        case TERMS_FETCHING_FAIL:
            return { ...state, loading: false };
        default:
            return state;
    }
} 