import { TERMS_FETCHING, TERMS_FETCHING_SUCCESS, TERMS_FETCHING_FAIL } from '../actionTypes'
import { terms } from '../../../services'


export const getTerms = () => {
    return async (dispatch) => {
        dispatch({ type: TERMS_FETCHING })
        const { result, status } = await terms()
        //console.log(result)
        if (status == 200) {
            dispatch({ type: TERMS_FETCHING_SUCCESS, payload: result.terms })
        } else {
            dispatch({ type: TERMS_FETCHING_FAIL })
        }
    }

}