import {
    PROFILE_FETCHING,
    PROFILE_FETCHING_SUCCESS,
    PROFILE_FETCHING_FAIL,
    POSTS_FETCHING_SUCCESS,
    FAVORITE_FETCHING_SUCCESS,
    NEWS_FETCHING_SUCCESS,
    USER_LOGOUT_SUCCESS,
    USER_LOGOUT_FAIL
} from '../actionTypes'
import { fetch_profile, logout, fetch_posts,fetch_favorite,fetch_news } from '../../../services'


export const getProfile = () => {
    return async (dispatch) => {
        dispatch({ type: PROFILE_FETCHING })
        const { result, status } = await fetch_profile()
        // /console.log(result, status)
        if (status == 200) {
            dispatch({ type: PROFILE_FETCHING_SUCCESS, payload: result })
        } else {
            dispatch({ type: PROFILE_FETCHING_FAIL })
        }

    }

}

export const getPosts = () => {
    return async (dispatch) => {
        dispatch({ type: PROFILE_FETCHING })
        const { result, status } = await fetch_posts()
        console.log(result, status)
        if (status == 200) {
            dispatch({ type: POSTS_FETCHING_SUCCESS, payload: result })
        } else {
            dispatch({ type: PROFILE_FETCHING_FAIL })
        }

    }
}

export const getNews = () => {
    return async (dispatch) => {
        dispatch({ type: PROFILE_FETCHING })
        const { result, status } = await fetch_news()
        console.log(result, status)
        if (status == 200) {
            dispatch({ type: NEWS_FETCHING_SUCCESS, payload: result })
        } else {
            dispatch({ type: PROFILE_FETCHING_FAIL })
        }

    }
}

export const getFavorite = () => {
    return async (dispatch) => {
        dispatch({ type: PROFILE_FETCHING })
        const { result, status } = await fetch_favorite()
        console.log(result, status)
        if (status == 200) {
            dispatch({ type: FAVORITE_FETCHING_SUCCESS, payload: result })
        } else {
            dispatch({ type: PROFILE_FETCHING_FAIL })
        }

    }
}

export const user_logout = () => {
    return async (dispatch) => {
        dispatch({ type: PROFILE_FETCHING })
        const { result, status } = await logout()
        console.log(result,status)
        // if (status == 200) {
        //     dispatch({ type: USER_LOGOUT_SUCCESS })
        // } else {
        //     dispatch({ type: USER_LOGOUT_FAIL })
        // }
    }
}
