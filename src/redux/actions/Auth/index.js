import { LOADING_AUTH, ERROR_AUTH, LOGIN_SUCCESS, SETTINGS_FETCHING_FAIL, SIGNUP_SUCCESS, SETTINGS_FETCHING, SETTINGS_FETCHING_SUCCESS, SETTINGS_UPDATE_SUCCESS } from "../actionTypes";
import { login, signupComplete, settingsData, signup } from "../../../services";
import { AsyncStorage } from 'react-native';
import { loginValidation, completeValdation } from "../../../utils/validation";

export const loginWithEmail = ({ email, password, navigation }) => {
    return async (dispatch) => {
        const valid = loginValidation({ email, password });
        if (valid.status == 'success') {
            dispatch({ type: LOADING_AUTH });
            const { status, result } = await login({ email, password });
            console.log(status, result);
            if (status == 200) {
                handleLoginSuccess({ request: result, navigation, dispatch });
            } else {
                dispatch({ type: ERROR_AUTH, payload: result.message })
            }
        } else {
            dispatch({ type: ERROR_AUTH, payload: valid.message })
        }
    }
}

export const signupWithEmail = ({ email, password, navigation }) => {
    return async (dispatch) => {

        const valid = loginValidation({ email, password });
        if (valid.status == 'success') {
            dispatch({ type: LOADING_AUTH });
            const { status, result } = await signup({ email, password });
            console.log(status, result);
            if (status == 200) {
                dispatch({ type: SIGNUP_SUCCESS, payload: result.message })
            } else {
                dispatch({ type: ERROR_AUTH, payload: result.message })

            }
        } else {
            dispatch({ type: ERROR_AUTH, payload: valid.message })
        }
    }
}



export const completeSignUp = ({ userData, navigation }) => {
    return async (dispatch) => {
        dispatch({ type: SETTINGS_FETCHING });
        const valid = completeValdation({ ...userData })
        if (valid.status == 'success') {
            const { status, result } = await signupComplete(userData);
            console.log(status, result);
            if (status == 200) {
                dispatch({ type: SETTINGS_UPDATE_SUCCESS })

                navigation.navigate('HomeScreen');
            } else {
                dispatch({ type: SETTINGS_FETCHING_FAIL })
            }

        } else {
            dispatch({ type: SETTINGS_FETCHING_FAIL })
        }
    }
}


export const getSettingsData = () => {
    return async (dispatch) => {
        dispatch({ type: SETTINGS_FETCHING })
        const request = await settingsData();
        console.log(request)
        if (request.status == 200) {
            dispatch({ type: SETTINGS_FETCHING_SUCCESS, payload: request.result })
        } else {
            dispatch({ type: SETTINGS_FETCHING_FAIL })
        }
    }
}


const handleLoginSuccess = async ({ request, navigation, dispatch }) => {
    await AsyncStorage.setItem('@USER_TOKEN', JSON.stringify(request.token)).then(() => {
        console.log(request)
        if (request.is_first_time) {
            navigation.navigate('CompleteSignUpScreen')
        } else {
            navigation.navigate('HomeScreen')
        }
        dispatch({ type: LOGIN_SUCCESS })

    });
}