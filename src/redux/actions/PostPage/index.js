import { UPLOAD_IMAGE } from "../actionTypes";
import { uploadPost } from "../../../services";

export const upload_post = ({ caption, category, imageFile, navigation }) => {

    return async (dispatch) => {
        dispatch({ type: UPLOAD_IMAGE })
        const { result, status } = await uploadPost({ caption, category, imageFile })
        console.log(result, status)
        if (status == 200) {
            navigation.navigate("HomeScreen")

        } else {
            alert("Upload Faild")
        }
    }
}


