import {
    HOME_FETCHIG,
    HOME_FETCHIG_SUCCESS,
    HOME_FETCHIG_FAIL,
    HOME_FAVORITE_FAIL,
    HOME_FAVORITE_SUCCESS,
    HOME_CATEGORIES_SUCCESS,
    HOME_CATEGORIES_FAIL,
    HOME_COMMENT_SUCCESS,
    HOME_COMMENT_FAIL,
    HOME_TOP_SUCCESS,
    HOME_TOP_FAIL,
    LOADING
} from '../actionTypes'
import { fetch_post, rate_post, addToFavorite, categories, addComment, topRate, blockPost,reportPost } from '../../../services'

export const getPost = (category_id, post_id) => {
    return async (dispatch) => {
        dispatch({ type: HOME_FETCHIG });
        get_new_post({ dispatch, post_id, category_id })
    }
}
export const ratePost = ({ value, post_id, category_id }) => {
    return async (dispatch) => {
        dispatch({ type: HOME_FETCHIG });
        const { result, status } = await rate_post({ value, id: post_id });
        //  console.log(result,status)
        if (status == 200) {
            get_new_post({ dispatch, post_id, category_id })
        } else {
            dispatch({ type: HOME_FETCHIG_FAIL })
        }
    }
}
const get_new_post = async ({ dispatch, post_id, category_id }) => {
    const { result, status } = await fetch_post(post_id, category_id)
    if (status == 200) {
        dispatch({ type: HOME_FETCHIG_SUCCESS, payload: result })
    } else {
        dispatch({ type: HOME_FETCHIG_FAIL })
    }
}

export const add_to_favorite = ({ id }) => {
    return async (dispatch) => {
        dispatch({ type: HOME_FETCHIG });
        const { result, status } = await addToFavorite({ id })
        if (status == 200) {
            dispatch({ type: HOME_FAVORITE_SUCCESS, payload: id })

        }
        else {
            dispatch({ type: HOME_FAVORITE_FAIL })
        }
    }
}


export const fetch_categories = () => {
    return async (dispatch) => {
        dispatch({ type: HOME_FETCHIG });
        const { result, status } = await categories()
        if (status == 200) {
            dispatch({ type: HOME_CATEGORIES_SUCCESS, payload: result })
        }
        else {
            dispatch({ type: HOME_CATEGORIES_FAIL })
        }
    }
}

export const add_comment = ({ id, userComment }) => {
    return async (dispatch) => {
        dispatch({ type: HOME_FETCHIG });
        const { result, status } = await addComment({ id, userComment })

        if (status == 200) {
            dispatch({ type: HOME_COMMENT_SUCCESS, payload: id, userComment })
        }
        else {
            dispatch({ type: HOME_COMMENT_FAIL })
        }
    }
}

export const top_rate = (id) => {

    return async (dispatch) => {
        dispatch({ type: HOME_FETCHIG });
        const { result, status } = await topRate(id)
        console.log(status, result)

        if (status == 200) {
            dispatch({ type: HOME_TOP_SUCCESS, payload: result.id })
        }
        else {
            dispatch({ type: HOME_TOP_FAIL })
        }
    }
}

export const block_post = (id) => {
    return async (dispatch) => {
        const {  status } = await blockPost(id)
        console.log(status)

        if (status == 200) {
            dispatch({ type: LOADING })
        }
        else {
            dispatch({ type: HOME_FETCHIG_FAIL })
        }
    }
}

export const report_post = (id) => {
    return async (dispatch) => {
        const {  status } = await reportPost(id)
        console.log(status)

        if (status == 200) {
            dispatch({ type: LOADING })
        }
        else {
            dispatch({ type: HOME_FETCHIG_FAIL })
        }
    }
}