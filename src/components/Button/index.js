import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { COLORS } from '../../common';
import PropTypes from 'prop-types';

const Button = ({ title, style, titleStyle, disabled, ...rest }) => {
    return (
        <TouchableOpacity disabled={disabled} style={[styles.container, style]} {...rest}  >
            <Text style={[styles.titleStyle, titleStyle]} >{title}</Text>
        </TouchableOpacity>
    );
}


Button.propTypes = {
    //onPress: PropTypes.func.isRequired,
  title:PropTypes.string.isRequired,
  titleStyle:PropTypes.any,
  style:PropTypes.any
}

export { Button };


const styles = StyleSheet.create({
    container: {
        height: 40,
        width: "70%",
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleStyle: {
        color: "#000"
    }
});