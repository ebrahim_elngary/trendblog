import React from 'react';
import { View, Modal, StyleSheet } from 'react-native';
import { Spinner } from '../Spinner';
import { COLORS } from '../../common';


const LoadingModal = ({ visible }) => {
    return <Modal
        visible={visible}
        transparent={true}
        style={styles.modal}
        onRequestClose={() => { }}
    >
        <View style={styles.container} >

            <Spinner color={COLORS.main}  size={"large"} />
        </View>
    </Modal>
}

export { LoadingModal };




const styles = StyleSheet.create({
    modal: {
        flex: 1
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
})