import React,{Component} from 'react';
import {ImageBackground,StyleSheet} from 'react-native';
import { Spinner } from '../Spinner';
import { IMAGES, COLORS } from '../../common';

import PropTypes from 'prop-types';

class ImagePlaceholder extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoaded : false,
            error:false
        }
    }



    render(){
        const {resizeMode, source, children, style} = this.props;
        const {isLoaded, error } = this.state;
        return(
            <ImageBackground
                onLoadEnd={ ()=> this.setState({ isLoaded : true }) }
                onError={ ()=> this.setState({ error : true }) }
               // source={ (isLoaded&& !error && source ) ? source : IMAGES.placeholderImage }
                style={[styles.container, style]}
                resizeMode={resizeMode || 'cover'}>
                {
                    (isLoaded && !error) ? children :
                        <Spinner  color={"red"} />
                }
            </ImageBackground>
        );
    }
}

ImagePlaceholder.proptypes = {
    source : PropTypes.string.isRequired,
    resizeMode : PropTypes.string,
    children : PropTypes.node,
    style : PropTypes.any
 }


export {ImagePlaceholder};


const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        justifyContent:'center',
    }
});