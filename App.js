import React, { Component } from 'react';
import { Provider } from 'react-redux';
import storeCreator from './src/redux/store';
import Navagtion from './src/navigation'


export default class App extends Component {
  render() {
    return (
      <Provider store={storeCreator} >
        <Navagtion />
      </Provider>
    );
  }
}
